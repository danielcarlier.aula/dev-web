## VSCode
* https://code.visualstudio.com/docs
* https://marketplace.visualstudio.com/vscode

## Python
* https://docs.python.org/3/
* https://www.python.org/downloads/
* https://pypi.org/

## Chrome
* https://www.google.com/intl/pt-BR/chrome/
* https://support.google.com/chrome/?hl=pt-BR
* https://chrome.google.com/webstore/category/extensions?hl=pt-br

## Firefox
* https://www.mozilla.org/pt-BR/firefox/new/
* https://support.mozilla.org/pt-BR/products/firefox/get-started
* https://addons.mozilla.org/pt-BR/firefox/extensions/

## Gitlab
* https://gitlab.com/i2311/desenvolvimento-web/material-aulas-2022-2

## Sever Python
``` shell
python -m http.server
```
