let minha_variavel = [1, 2, 3, 4, "casa"];
let outra_variavel = [5, 3, 4, 21];
const nova_variavel = [
  {
    nome: "um",
    valor: 1,
    valor_real: 1.0,
  },
  {
    nome: "dois",
    valor: 2,
    valor_real: 2.0,
  },
  {
    nome: "três",
    valor: 3,
    valor_real: 3.0,
  },
];

const imprime = (entrada) => {
  let saida = "";
  for (let i = 0; i < entrada.length; i++) {
    saida = saida + entrada[i];
    if (i != entrada.length - 1) {
      saida = saida + "-";
    }
  }

  return saida + ".";
};

const imprimeForEach = (entrada) => {
  let saida = "";
  entrada.forEach((elemento, indice) => {
    saida += elemento;
    if (indice < entrada.length - 1) saida += "-";
  });

  return saida + ".";
};

const imprimeForOf = (entrada) => {
  let saida = "";
  let count = 0;
  for (const elemento of entrada) {
    saida += count < entrada.length - 1 ? elemento + "-" : elemento;
    count++;
  }

  return saida + ".";
};

const incluiImagem = (entrada) => {
  return entrada.map((elemento, indice) => {
    elemento.imagem = indice + 1 + ".png";
    return elemento;
  });
};

const filtraLista = (lista, pesquisa) => {
  let nome_limpo;
  let deve_entrar;

  const nova_lista = lista.filter((elemento) => {
    nome_limpo = elemento.nome.toLowerCase();
    deve_entrar = nome_limpo.includes(pesquisa.toLowerCase());
    return deve_entrar;
  });

  return nova_lista;
};

window.onload = () => {};
