## Porque as portas padrão mudaram ao longo do tempo

Para cumprir as recomendações da Autoridade de Números Atribuídos à Internet (IANA), a Microsoft aumentou o intervalo dinâmico de porta do cliente para conexões de saída no Windows Vista e no Windows Server 2008. A nova porta inicial padrão é 49152 e a nova porta final padrão é 65535. Esta é uma alteração da configuração de versões anteriores do Windows que usavam um intervalo de porta padrão de 1025 a 5000.

REF: https://docs.microsoft.com/pt-br/troubleshoot/windows-server/networking/default-dynamic-port-range-tcpip-chang#introduction

## Portas padrões e mais comuns e seus protocolos

Ao todo, é possível usar 65536 portas TCP e UDP, começando em 1. Tanto no protocolo TCP como no UDP, é comum o uso das portas de 1 a 1024, já que a aplicação destas é padronizada pela IANA (Internet Assigned Numbers Authority). De acordo com essa entidade, eis algumas das portas TCP mais utilizadas:

* 21 - FTP;
* 23 - Telnet;
* 25 - SMTP;
* 80 - HTTP;
* 110 - POP3;
* 143 - IMAP;
* 443 - HTTPS.

REF: https://www.infowester.com/portastcpudp.php

