let minha_variavel = [1,2,3,4,5,'casa'];

const imprime = (entrada) => {
    let saida = "";
    for(let i = 0; i < entrada.length; i++) {
        if((i+1) == entrada.length) {
            saida = saida + entrada[i];
        } else {
            saida = saida + entrada[i] + '-';
        }
    }

    return saida;
}

const imprimeForEach = (entrada) => {
    let saida = "";
    entrada.forEach( (elemento, index) => {
        saida += elemento;
        if (index < entrada.length-1) {
            saida += '-';
        }
    });

    return saida += '.';
}

const imprimeForOf = (entrada) => {
    let saida = '';
    let count = 0;
    for (const elemento of entrada) {
        (count < entrada.length - 1)? saida += elemento + '-': saida += elemento;
        count++;
    }

    return saida + '.';
}

window.onload = () => {

}
