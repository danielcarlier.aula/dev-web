let meu_vetor = ['primeiro elemento', 31];
console.log('meu vetor.length', meu_vetor.length);

let outro_vetor = new Array();
console.log('outro_vetor.length', outro_vetor.length);
outro_vetor[0] = 'outro primeiro elemento';
outro_vetor[1] = 'outro segundo elemento';
outro_vetor[2] = 7;
console.log('outro_vetor.length', outro_vetor.length);

const func_while = ( entrada ) => {
    let contador = 0;
    while (contador < entrada.length) {
        console.log(entrada[contador]);
        contador++;
    }
}

function func_for( entrada ) {
    let achou = false;
    for(let i = 0; i < entrada.length; i++) {
        if(entrada[i] > 30) {
            console.log(entrada[i]);
            achou = true;
        } else {
            console.log('--');
        }
    }

    if(!achou) {
        console.log('Não achou o valor, campeão')
    }
}

function aviso() {
    alert("Opaa");
}

function clicaBotao(inputText) {
    alert(inputText);
}

const muda_texto = () => {
    let texto = document.getElementById("meu_texto").value;
    
    document.getElementById("primeiro_span").innerHTML = texto;
}

window.onload = function(){
    document.querySelector("body > div:nth-child(3) > ol:nth-child(1) > li:nth-child(7)").addEventListener("mouseover", aviso);

    const botao = document.createElement('input');
    botao.type = 'button';
    botao.value = 'Testar';
    botao.addEventListener("onclick", clicaBotao("56"));
    document.getElementById('inputs').appendChild(botao);
}
