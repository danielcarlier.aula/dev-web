// Lista com todas as montadoras pre-definidas
let montadoras = [
    "chevrolet",
    "volkswagen",
    "hyundai",
    "jeep",
    "toyota",
    "renault",
    "honda",
    "peugeot",
    "nissan"
]

//Troca_cor seleciona o elemento e altera sua cor conforme a selecionada
function Troca_cor() {
    let cor = document.getElementById("cores_select").value;
    document.getElementById("barra_colorida").style.backgroundColor = cor;
}

// Atualiza_lista_montadoras atualiza o menu dropdown de montadoras disponíveis
function Atualiza_lista_montadoras() {
    let qtd_montadoras_selecionadas = document.getElementById("quantidade_select").value;

    document.getElementById("montadora_select").replaceChildren();
    let montadora_inicial = document.createElement("option");
    montadora_inicial.value = "fiat";
    montadora_inicial.text = "fiat";
    document.getElementById("montadora_select").appendChild(montadora_inicial);

    for(let i = 0; i < (qtd_montadoras_selecionadas-1); i++) {
        const opt = document.createElement("option");
        opt.value = montadoras[i]
        opt.text = montadoras[i]
        document.getElementById("montadora_select").appendChild(opt);
    }
}

// Atualiza_nome_montadora atualiza o campo que exibe apenas o nome da montadora selecionada
function Atualiza_nome_montadora() {
    let montadora_selecionada = document.getElementById("montadora_select").value;
    document.getElementById("nome_montadora").textContent = montadora_selecionada;
}

// Add_nova_montadora adiciona uma nova montadora à lista já existente
function Add_nova_montadora() {
    let nova_montadora = document.getElementById("nova_montadora_input").value;
    if(nova_montadora == "") {
        alert("Montadora vazia não é permitido!");
        return
    }

    document.getElementById("quantidade_select").max++
    let qtd_montadoras = document.getElementById("quantidade_select").max;
    montadoras.push(nova_montadora);
    document.getElementById("nova_montadora_input").value = "";
    document.getElementById("nova_montadora_input").text = "";
    alert("Montadora \"" + nova_montadora + "\" adicionada com sucesso!\nAgora são: " + qtd_montadoras + " montadoras no total.");
}

// Excluir_montadora remove a última montadora da lista
function Excluir_montadora() {
    let montadora_excluida = montadoras[-1];
    montadoras.pop;
    document.getElementById("quantidade_select").max--
    let qtd_montadoras = document.getElementById("quantidade_select").max;
    document.getElementById("quantidade_select").value = qtd_montadoras;
    document.getElementById("nome_montadora").textContent = "fiat";
    Atualiza_lista_montadoras();
    alert("Montadora \"" + montadora_excluida + "\" excluída com sucesso!\nAgora são: " + qtd_montadoras + " montadoras no total.");
}
